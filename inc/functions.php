<?php
/**
 * @file - Supporting functions for functionality.
 */

/**
 * Makes request (GET, POST) to Facebook API.
 *
 * @url - string with url address to Facebook API.
 * @jsonData - json Object containing data for API.
 * @is_post_request - boolean.
 *
 * @return - object.
 */
function make_facebook_api_request($url, $jsonData = NULL, $is_post_request = FALSE) {
  //Initiate cURL.
  $ch = curl_init($url);

  if ($is_post_request) {
    //Encode the array into JSON.
    $jsonDataEncoded = $jsonData;

    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);

    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  }
  else {
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    //curl_setopt($ch,CURLOPT_HEADER, false);
  }

  //Execute the request
  $result = curl_exec($ch);
  return $result;
}

/**
 * Extract from message data needed to perfom chat-bot reservation block fill.
 *
 * @original_message - string containing message sent by Facebook user.
 *
 * @return - array|boolean.
 */
function extract_data_from_message($original_message) {
  $response = FALSE;

  if ($original_message == "") {
    //return 'Error 1';
    return FALSE;
  }

  $original_message_exploded_arr = explode(' ', trim($original_message));
  $credentials_part = isset($original_message_exploded_arr[0]) ? $original_message_exploded_arr[0] : FALSE;
  $date_part        = isset($original_message_exploded_arr[1]) ? $original_message_exploded_arr[1] : FALSE;
  $time_part        = isset($original_message_exploded_arr[2]) ? $original_message_exploded_arr[2] : FALSE;

  if (!$credentials_part || !$date_part || !$time_part) {
    //return 'Error 2';
    return FALSE;
  }

  // Credentials
  $credentials_part_temp = explode('|', trim($credentials_part));
  $id_pitch              = isset($credentials_part_temp[0]) ? $credentials_part_temp[0] : FALSE;
  $id_pitch_employee     = isset($credentials_part_temp[1]) ? $credentials_part_temp[1] : FALSE;

  // Date
  $date_part_temp  = explode('.', trim($date_part));
  $date_part_day   = isset($date_part_temp[0]) ? $date_part_temp[0] : FALSE;
  $date_part_month = isset($date_part_temp[1]) ? $date_part_temp[1] : FALSE;
  $date_part_year  = isset($date_part_temp[2]) ? $date_part_temp[2] : FALSE;

  // Time
  $time_part_temp    = explode(':', trim($time_part));
  $time_part_hour    = isset($time_part_temp[0]) ? $time_part_temp[0] : FALSE;
  $time_part_minutes = isset($time_part_temp[1]) ? $time_part_temp[1] : FALSE;

  if (!$id_pitch || !$id_pitch_employee || !$date_part_day || !$date_part_month ||
      !$date_part_year || !$time_part_hour || !$time_part_minutes) {

    //return 'Error 3';
    return FALSE;
  }

  // Format response.
  $response['pitch_id']            = $id_pitch;
  $response['reservation_day']     = $date_part_day;
  $response['reservation_month']   = $date_part_month;
  $response['reservation_year']    = $date_part_year;
  $response['reservation_hour']    = $time_part_hour;
  $response['reservation_minutes'] = $time_part_minutes;

  //return 'All ok.';
  return $response;
}

/**
 * Creates PDO object needed for communication with database.
 *
 * @db_conn_data - array of parameters for connection
 *
 * @return object|boolean
 */
function get_database_connection(array $db_conn_data) {
  $db_servername = isset($db_conn_data['db_servername']) ? $db_conn_data['db_servername'] : '';
  $db_name       = isset($db_conn_data['db_name'])       ? $db_conn_data['db_name']       : '';
  $db_username   = isset($db_conn_data['db_username'])   ? $db_conn_data['db_username']   : '';
  $db_password   = isset($db_conn_data['db_password'])   ? $db_conn_data['db_password']   : '';

  try {
    $conn = new PDO("mysql:host=$db_servername;dbname=$db_name", $db_username, $db_password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
  }
  catch(PDOException $e) {
    return FALSE;
    return $e->getMessage();
  }
}

/**
 * Creates custom table (if it doesn't exist) needed for this
 * functionality to work properly.
 *
 * @conn - object PDOConnection object.
 *
 * @return - boolean.
 */
function create_db_table_if_non_existant($conn) {
  $sql = "CREATE TABLE IF NOT EXISTS ge_bot_block_fill(
     id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
     pitch_id VARCHAR(5) NOT NULL,
     employee_id VARCHAR(5) NOT NULL,
     active BOOLEAN DEFAULT true);";

  try {
    $conn->exec($sql);
    return TRUE;
  }
  catch (PDOException $e) {
    return FALSE;
    return $e->getMessage();
  }
}

/**
 * Insert data required for chat-bot block fill into
 * custom table.
 *
 * @conn - object PDOConnection object.
 * @pitch_id - integer id of pitch.
 * @employee_id - integer id of employee on that pitch.
 * @active - boolean if the employee is active.
 *
 * @return - boolean.
 */
function insert_data_into_custom_table($conn, $pitch_id, $employee_id, $active = TRUE) {
  $statement = $conn->prepare('INSERT INTO ge_bot_block_fill (pitch_id, employee_id, active)
    VALUES (?, ?, ?)');

  try {
    $statement->execute([$pitch_id, $employee_id, $active]);
    return TRUE;
  }
  catch(PDOException $e) {
    return FALSE;
    return $e->getMessage();
  }

  // Just in case.
  return FALSE;
}

/**
 * Performs check if the user has access to that pitch.
 *
 * @conn - object PDOConnection object.
 * @pitch_id - integer id of pitch.
 * @employee_id - integer id of employee on that pitch.
 *
 * @return - array|boolean.
 */
function check_user_credentials($conn, $pitch_id, $employee_id) {
  $stmt = $conn->prepare("SELECT pitch_id, employee_id FROM ge_bot_block_fill WHERE pitch_id=:pitch_id AND employee_id=:employee_id AND active=1");
  $stmt->execute(['pitch_id' => $pitch_id, 'employee_id' => $employee_id]);
  $data = $stmt->fetch();

  if (isset($data['pitch_id'])) {
    unset($data[0], $data[1]);
    return $data;
  }

  return FALSE;
}

/**
 *  Process message received from sender.
 *
 * @received_message - string containing message recevied via FB Messanger.
 *
 * @return - string.
 */
function process_received_message($received_message) {
$message_to_reply = "";

  if(preg_match('[vreme|sati|trenutno vreme|sada|   ^`     ^o| ^g   ^a]', strtolower($received_message))) {
    $time  = getdate();
    $hours = $time['hours'];
    if ($time['minutes'] < 10) {
      $minutes = "0".$time['minutes'];
    }
    else {
      $minutes = $time['minutes'];
    }

    $response = $hours.":".$minutes;
    if($response != '') {
      $message_to_reply = $response;
    }
    else {
      $message_to_reply = "Na  alost, ne znam.";
    }
  }
  elseif (preg_match('[kako|gde|zakazem|termin|   ^`     ^o| ^g   ^a]', strtolower($received_message))) {
    $message_to_reply = "Spisak terena koji mo  ete iznajmiti mo  ete pogledati ovde: https://pitch.coworks.be/tereni/";
  }
  else {
    $message_to_reply = 'Na  alost, nemam odgovor na Va  e pitanje. ';
  }

  return $message_to_reply;
}
