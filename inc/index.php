<?php
// For debuging, uncomment this part.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Get external files, initial parameters.
require_once 'config.php';
require_once 'functions.php';
$hub_verify_token = null;

$conn = get_database_connection($db_conn_data);
// Create custom table required for this code to work.
// It should be called only once, on new database.
/*
if ($conn != FALSE) {
  $temp = create_db_table_if_non_existant($conn);
  if ($temp) {
    echo 'New table successfully created.';
  }
  else {
    echo 'Error while creating new custom table.';
  }
}
*/

// Insert pitch and employee data into custom table.
/*
if ($conn != FALSE) {
  $pitch_id    = '1111';
  $employee_id = '111';
  $temp        = insert_data_into_custom_table($conn, $pitch_id, $employee_id);
  if ($temp) {
    echo "Successfully inserted data into custom table.";
  }
  else {
    echo "Error while inserting data into custom table.";
  }
}
*/

// Verify app(page) token.
if(isset($_REQUEST['hub_challenge'])) {
  $challenge        = $_REQUEST['hub_challenge'];
  $hub_verify_token = $_REQUEST['hub_verify_token'];
}
if ($hub_verify_token === $verify_token) {
  echo $challenge;
}

// Process message input.
$input            = json_decode(file_get_contents('php://input'), TRUE);
$sender           = $input['entry'][0]['messaging'][0]['sender']['id'];
$message          = $input['entry'][0]['messaging'][0]['message']['text'];
//$message_to_reply = '';
$message_to_reply = extract_data_from_message($message);
if ($message_to_reply == FALSE) {
  $message_to_reply = process_received_message($message);
}
elseif(is_array($message_to_reply)) {
  // Perform filling of the block.
}

//API Url
$url = 'https://graph.facebook.com/v3.2/me/messages?access_token=' . $access_token;
//The JSON data.
$jsonData = '{
    "recipient":{
        "id":"'.$sender.'"
    },
    "message":{
        "text":"'.$message_to_reply.'"
    }
}';

make_facebook_api_request($url, $jsonData, TRUE);
