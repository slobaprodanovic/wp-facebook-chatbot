<?php
/**
 * Plugin Name: GE Facebook Chatbot
 * Plugin URI: https://www.sportski-tereni.rs/
 * Description: Facebook chatbot functionality for specific project.
 * Version: 1.0
 * Author: Good Enough Agency
 * Author URI: https://www.coworks.be/
 **/

/**
 * Call function that implements this functionality.
 */
add_action( 'init', ge_perform_functionality());

/**
 * Main function that implements this functionality.
 *
 * @return null
 */
function ge_perform_functionality() {
  if (!ge_check_if_specific_url()) {
    return;
  }

  return;
}

/**
 * This plugin should work only on one specific page. This fn checks if that
 * specific page is requested.
 *
 * @return bool
 */
function ge_check_if_specific_url() {
  $request_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "";

  if ($request_url == '/chatbot-hooks') {
    return TRUE;
  }

  return FALSE;
}